import 'package:animation_example/menu_animation_example.dart';
import 'package:flutter/material.dart';

const MIN_WIDTH = 0.0;
const MAX_WIDTH = 80.0;

class LoadingAnimationExample2 extends StatefulWidget {
  @override
  _LoadingAnimationExample2State createState() => _LoadingAnimationExample2State();
}

class _LoadingAnimationExample2State extends State<LoadingAnimationExample2> {
  AnimationState animationState = AnimationState.OUT;

  @override
  Widget build(BuildContext context) {
    Tween tween;
    switch (animationState) {
      case AnimationState.IN:
        tween = Tween<double>(begin: MAX_WIDTH, end: MIN_WIDTH);
        break;
      case AnimationState.OUT:
        tween = Tween<double>(begin: MIN_WIDTH, end: MAX_WIDTH);
        break;
    }

    return TweenAnimationBuilder<double>(
      tween: tween,
      duration: Duration(milliseconds: 550),
      curve: Curves.decelerate,
      onEnd: () {
        setState(() {
          if (animationState == AnimationState.IN) {
            animationState = AnimationState.OUT;
          } else {
            animationState = AnimationState.IN;
          }
        });
      },
      builder: (BuildContext cont, double width, Widget w) {
        return _buildBody(width);
      },
    );
  }

  Widget _buildBody(double width) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _getloadingLine(width, Colors.blue[300]),
        _getloadingLine(MAX_WIDTH - width, Colors.greenAccent),
      ],
    );
  }

  Widget _getloadingLine(double width, Color color) {
    double animatedWidth = width;

    return Container(
      height: 5,
      width: animatedWidth,
      color: color,
    );
  }
}
