import 'package:animation_example/color_animation_example.dart';
import 'package:animation_example/loading_animation_example.dart';
import 'package:animation_example/loading_animation_example2.dart';
import 'package:animation_example/menu_animation_example.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Animation Examples',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        body: Examples(),
      ),
    );
  }
}

class Examples extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: Column(
        children: [
          MenuAnimationExample(),
          SizedBox(height: 100),
          LoadingAnimationExample(),
          SizedBox(height: 100),
          LoadingAnimationExample2(),
          SizedBox(height: 100),
          WarningAnimationExample(),
        ],
      ),
    );
  }
}
