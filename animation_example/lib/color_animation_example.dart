import 'package:animation_example/menu_animation_example.dart';
import 'package:flutter/material.dart';

const MIN = 0.0;
const MAX = 255.0;

class WarningAnimationExample extends StatefulWidget {
  @override
  _WarningAnimationExampleState createState() => _WarningAnimationExampleState();
}

class _WarningAnimationExampleState extends State<WarningAnimationExample> {
  AnimationState animationState = AnimationState.OUT;

  @override
  Widget build(BuildContext context) {
    Tween tween;
    switch (animationState) {
      case AnimationState.IN:
        tween = Tween<double>(begin: MAX, end: MIN);
        break;
      case AnimationState.OUT:
        tween = Tween<double>(begin: MIN, end: MAX);
        break;
    }

    return TweenAnimationBuilder<double>(
      tween: tween,
      duration: Duration(milliseconds: 550),
      curve: Curves.linear,
      onEnd: () {
        setState(() {
          if (animationState == AnimationState.IN) {
            animationState = AnimationState.OUT;
          } else {
            animationState = AnimationState.IN;
          }
        });
      },
      builder: (BuildContext cont, double colorNum, Widget w) {
        return Center(child: _getCube(colorNum.floor()));
      },
    );
  }

  Widget _getCube(int colorNum) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(30)),
        color: Color.fromARGB(255, 230, colorNum, colorNum),
      ),
      height: 42,
      width: 130,
      child: Center(
        child: Text(
          "WARNING",
          style: TextStyle(color: Colors.white, fontSize: 22),
        ),
      ),
    );
  }
}
