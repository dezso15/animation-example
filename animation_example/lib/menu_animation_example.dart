import 'package:flutter/material.dart';

enum AnimationState {
  IN,
  OUT,
}

const MIN_HEIGHT = 35.0;
const MAX_HEIGHT = 60.0;

class MenuAnimationExample extends StatefulWidget {
  @override
  _MenuAnimationExampleState createState() => _MenuAnimationExampleState();
}

class _MenuAnimationExampleState extends State<MenuAnimationExample> {
  AnimationState animationState = AnimationState.IN;

  @override
  Widget build(BuildContext context) {
    Tween tween;

    if (animationState == AnimationState.OUT) {
      tween = Tween<double>(begin: MIN_HEIGHT, end: MAX_HEIGHT);
    } else {
      tween = Tween<double>(begin: MAX_HEIGHT, end: MIN_HEIGHT);
    }

    return TweenAnimationBuilder<double>(
      tween: tween,
      duration: Duration(milliseconds: 200),
      curve: Curves.bounceIn,
      builder: (BuildContext cont, double height, Widget w) {
        return _buildBody(height);
      },
    );
  }

  Widget _buildBody(double height) {
    return _getMouseRegion(
      Center(
        child: Container(
          height: height,
          width: double.infinity,
          color: Colors.blue,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text("Home", style: TextStyle(color: Colors.white, fontSize: 22)),
              SizedBox(width: 20),
              Text("Login", style: TextStyle(color: Colors.white, fontSize: 22)),
              SizedBox(width: 20),
              Text("Contacts", style: TextStyle(color: Colors.white, fontSize: 22)),
              SizedBox(width: 20),
            ],
          ),
        ),
      ),
    );
  }

  Widget _getMouseRegion(Widget child) {
    return MouseRegion(
      onEnter: (s) {
        setState(() {
          animationState = AnimationState.OUT;
        });
      },
      onExit: (s) {
        setState(() {
          animationState = AnimationState.IN;
        });
      },
      child: child,
    );
  }
}
