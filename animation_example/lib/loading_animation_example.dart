import 'package:animation_example/menu_animation_example.dart';
import 'package:flutter/material.dart';

const MIN_WIDTH = 40.0;
const MAX_WIDTH = 80.0;

class LoadingAnimationExample extends StatefulWidget {
  @override
  _LoadingAnimationExampleState createState() => _LoadingAnimationExampleState();
}

class _LoadingAnimationExampleState extends State<LoadingAnimationExample> {
  AnimationState animationState = AnimationState.OUT;

  @override
  Widget build(BuildContext context) {
    Tween tween;
    switch (animationState) {
      case AnimationState.IN:
        tween = Tween<double>(begin: MAX_WIDTH, end: MIN_WIDTH);
        break;
      case AnimationState.OUT:
        tween = Tween<double>(begin: MIN_WIDTH, end: MAX_WIDTH);
        break;
    }

    return TweenAnimationBuilder<double>(
      tween: tween,
      duration: Duration(milliseconds: 550),
      curve: Curves.linear,
      onEnd: () {
        setState(() {
          if (animationState == AnimationState.IN) {
            animationState = AnimationState.OUT;
          } else {
            animationState = AnimationState.IN;
          }
        });
      },
      builder: (BuildContext cont, double width, Widget w) {
        return _buildBody(width);
      },
    );
  }

  Widget _buildBody(double width) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _getloadingLine(width, 0),
        SizedBox(height: 8),
        _getloadingLine(width, 1),
        SizedBox(height: 8),
        _getloadingLine(width, 0),
        SizedBox(height: 8),
        _getloadingLine(width, 1),
      ],
    );
  }

  Widget _getloadingLine(double width, int groupId) {
    double animatedWidth = width;
    if (groupId == 1) {
      animatedWidth = MIN_WIDTH + (MAX_WIDTH - width);
    }

    return Container(
      height: 3,
      width: animatedWidth,
      color: Colors.blue,
    );
  }
}
